package org.bespyatov.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("project.properties")
@Data
public class BotConfig {

    @Value("${botName}")
    String botName;

    @Value("${botToken}")
    String botToken;

}

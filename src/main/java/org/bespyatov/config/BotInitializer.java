package org.bespyatov.config;

import lombok.extern.slf4j.Slf4j;
import org.bespyatov.service.TelegramBot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.*;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

@Slf4j
@Component
public class BotInitializer {

    {
        ApiContextInitializer.init();
    }
    @Autowired
    TelegramBot bot;

    @EventListener({ContextRefreshedEvent.class})
    public void init() {

        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

        try {
            telegramBotsApi.registerBot(bot);
        } catch (TelegramApiRequestException e) {
            log.error("Error occurred: " + e.getMessage());
        }
    }
}

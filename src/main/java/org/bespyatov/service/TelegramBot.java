package org.bespyatov.service;

import lombok.extern.slf4j.Slf4j;
import org.bespyatov.config.BotConfig;
import org.bespyatov.keyboards.FAQInlineKeyboard;
import org.bespyatov.keyboards.TimeInlineKeyboard;
import org.bespyatov.keyboards.WeekInlineKeyboard;
import org.bespyatov.models.User;
import org.bespyatov.models.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;


@Slf4j
@Component
public class TelegramBot extends TelegramLongPollingBot {
    @Autowired
    private UserRepository userRepository;

    final BotConfig config;

    public TelegramBot(BotConfig config) {
        this.config = config;
    }

    @Override
    public String getBotUsername() {
        return config.getBotName();
    }

    @Override
    public String getBotToken() {
        return config.getBotToken();
    }

    @Override
    public void onUpdateReceived(Update update) {
        String date = new String();
        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText();
            Message message = update.getMessage();
            if ("Воскресенье Понедельник Вторник Среда Четверг Пятница Суббота".contains(message.getText())) {
                int dayWeek = WeekInlineKeyboard.getCustomersDate
                        (WeekInlineKeyboard.getDayWeekFromString(message.getText()));
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, dayWeek);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                date = sdf.format(calendar.getTime());
                sendMsg(message, "Для записи на " + date);
                sendMsg(message, "Пожалуйста, выберите удобное время");
            } if (".".contains(message.getText())) {
                registerUser(message, date, message.getText());
                sendMsg(message, "Благодарю Вас, " + update.getMessage().getChat().getFirstName() +
                        "! Вы записаны на " + date + " " + message.getText());
                sendMsg(message, "Если вдруг произойдут какие либо изменения, " +
                        "я сразу же с Вами свяжусь");
            } else {
                switch (messageText) {
                    case "/start":
                        startCommandReceived(message, update.getMessage().getChat().getFirstName());
                        break;
                    case "Заправка стационарного кондиционера":
                        sendMsg(message, "Обслуживание стационарного кондиционера 4000 рублей " +
                                "сюда включены: диагностика, чистка дренажа(при необходимости), " +
                                "заправка фреоном до 600 грамм, чистка фильтров внутреннего блока " +
                                "цена указана при условии доступа к внешнему блоку без применения " +
                                "альпинистского снаряжения, с применением альпинстского снаряжения +4000 рублей к указанной цене.");
                        break;
                    case "Заправка кондиционера авто":
                        sendMsg(message, "Цена зависит от количества фреона, 3000-3500 рублей " +
                                "сюда включены: диагностика, опрессовка азотом, поиск утечки если необходимо, " +
                                "заправка фреоном до 600 грамм с добавлением масла и ультрафиолетовой краски(по желанию) " +
                                "в случае, если обнаруживается утечка, сначала вы платите только за диагностику и поиск " +
                                "1500 рублей и после ее устранения оставшуюся сумму 1500-2000 рублей за заправку");
                        break;
                    case "Диагностика, поиск утечек":
                        sendMsg(message, "Для автомобилей диагностика и поиск утечки 1500 рублей");
                        break;
                    case "Записаться":
                        sendMsg(message, "Для записи, пожалуйста, выберите день");
                        break;
                    case "Если остались вопросы":
                        sendMsg(message, "Пожалуйста, свяжитесь со мной напрямую: +79533738973");
                        break;
                    default:
                        sendMsg(message, "Приветствую! Для получения информации пользуйтесь кнопками внизу экрана");
                }
            }
        }
    }


    private void registerUser(Message msg, String date, String time) {

        if (userRepository.findById(msg.getChatId()).isEmpty()) {

            var chatId = msg.getChatId();
            var chat = msg.getChat();

            User user = new User();

            user.setChatId(chatId);
            user.setFirstName(chat.getFirstName());
            user.setLastName(chat.getLastName());
            user.setUserName(chat.getUserName());
            user.setRegisteredAt(new Timestamp(System.currentTimeMillis()));
            user.setDateOfVisitService(date);

            userRepository.save(user);
            log.info("user saved: " + user);

        }


    }

    private void startCommandReceived(Message message, String name) {

        String answer = "Приветствую, " + name + " для ответа воспользуйтесь кнопками внизу экрана";
        log.info("Replied to user " + name);

        sendMsg(message, answer);
    }

    private void sendMsg(Message message, String text) {

        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);

        try {
            if (text.equals("Для записи, пожалуйста, выберите день")) {

                WeekInlineKeyboard inlineKeyboardWeek = new WeekInlineKeyboard();
                inlineKeyboardWeek.setButtons(sendMessage);

            } else if (text.equals("Пожалуйста, выберите удобное время")) {

                TimeInlineKeyboard timeInlineKeyboard = new TimeInlineKeyboard();
                timeInlineKeyboard.setButtons(sendMessage);

            } else {

                FAQInlineKeyboard faqInlineKeyboard = new FAQInlineKeyboard();
                faqInlineKeyboard.setButtons(sendMessage);
            }
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            log.error("Error occurred: " + e.getMessage());
        }
    }
}

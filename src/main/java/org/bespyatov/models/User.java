package org.bespyatov.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity(name = "usersDataTable")
public class User {


    @Id
    private Long chatId;
    private String firstName;

    public String getDateOfVisitService() {
        return dateOfVisitService;
    }

    public void setDateOfVisitService(String dateOfVisitService) {
        this.dateOfVisitService = dateOfVisitService;
    }

    private String lastName;
    private String userName;
    private String dateOfVisitService;
    private String timeOfVisitService;

    public String getTimeOfVisitService() {
        return timeOfVisitService;
    }

    public void setTimeOfVisitService(String timeOfVisitService) {
        this.timeOfVisitService = timeOfVisitService;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "User{" +
                "chatId=" + chatId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", registeredAt=" + registeredAt +
                '}';
    }

    public Timestamp getRegisteredAt() {
        return registeredAt;
    }

    public void setRegisteredAt(Timestamp registeredAt) {
        this.registeredAt = registeredAt;
    }

    private Timestamp registeredAt;

    public void setChatId(Long id) {
        this.chatId = id;
    }

    public Long getChatId() {
        return chatId;
    }
}

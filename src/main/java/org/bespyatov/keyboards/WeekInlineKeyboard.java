package org.bespyatov.keyboards;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.*;

public class WeekInlineKeyboard extends Date {
    static String[] WEEKDAYS = {"Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"};
    static String[] MONTHS = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};
    static Calendar rightNow = Calendar.getInstance();
    static Date date = rightNow.getTime();
    static int currentDayOfWeek = date.getDay();

    public void setButtons (SendMessage sendMessage) {

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboardRowList = new ArrayList<>();
        KeyboardRow sunday = new KeyboardRow();
        KeyboardRow monday = new KeyboardRow();
        KeyboardRow tuesday = new KeyboardRow();
        KeyboardRow wednesday = new KeyboardRow();
        KeyboardRow thursday = new KeyboardRow();
        KeyboardRow friday = new KeyboardRow();
        KeyboardRow saturday = new KeyboardRow();

        //rows with days of week
        sunday.add(WEEKDAYS[0]);
        monday.add(WEEKDAYS[1]);
        tuesday.add(WEEKDAYS[2]);
        wednesday.add(WEEKDAYS[3]);
        thursday.add(WEEKDAYS[4]);
        friday.add(WEEKDAYS[5]);
        saturday.add(WEEKDAYS[6]);

        keyboardRowList.add(sunday);
        keyboardRowList.add(monday);
        keyboardRowList.add(tuesday);
        keyboardRowList.add(wednesday);
        keyboardRowList.add(thursday);
        keyboardRowList.add(friday);
        keyboardRowList.add(saturday);

        replyKeyboardMarkup.setKeyboard(keyboardRowList);


//        //first row
//        monthYear.add("<");
//        monthYear.add(currentMonth + " " + getCurrentYear);
//        monthYear.add(">");
//
//        //second row
//        for (String i : WEEKDAYS) {
//            daysOfWeek.add(i);
//        }
//
//        //rows with dates
//        setDatesOnKeyboard(datesOfMonth1, 1, firstDayOfWeek, lastDayOfMonth);
//        setDatesOnKeyboard(datesOfMonth2, Integer.parseInt(datesOfMonth1.get(6).getText()) + 1, firstDayOfWeek, lastDayOfMonth);
//        setDatesOnKeyboard(datesOfMonth3, Integer.parseInt(datesOfMonth2.get(6).getText()) + 1, firstDayOfWeek, lastDayOfMonth);
//        setDatesOnKeyboard(datesOfMonth4, Integer.parseInt(datesOfMonth3.get(6).getText()) + 1, firstDayOfWeek, lastDayOfMonth);
//        setDatesOnKeyboard(datesOfMonth5, Integer.parseInt(datesOfMonth4.get(6).getText()) + 1, firstDayOfWeek, lastDayOfMonth);
//        if (!datesOfMonth5.get(6).getText().equals(" ")) {
//            setDatesOnKeyboard(datesOfMonth6, Integer.parseInt(datesOfMonth5.get(6).getText()) + 1, firstDayOfWeek, lastDayOfMonth);
//        }
//
//        keyboardRowList.add(monthYear);
//        keyboardRowList.add(daysOfWeek);
//        keyboardRowList.add(datesOfMonth1);
//        keyboardRowList.add(datesOfMonth2);
//        keyboardRowList.add(datesOfMonth3);
//        keyboardRowList.add(datesOfMonth4);
//        keyboardRowList.add(datesOfMonth5);
//        keyboardRowList.add(datesOfMonth6);
//
//        replyKeyboardMarkup.setKeyboard(keyboardRowList);
//    }
//
//    KeyboardRow setDatesOnKeyboard (KeyboardRow datesOfMonth, int dates, int firstDayOfWeek, int lastDayOfMonth) {
//        for (int i = 0; i < 7; i++) {
//            if ((i < firstDayOfWeek && dates < firstDayOfWeek) || (dates > lastDayOfMonth)) {
//                    datesOfMonth.add(" ");
//            } else {
//                datesOfMonth.add(String.valueOf(dates));
//                dates++;
//            }
//        }
//        return datesOfMonth;
//
    }

    public static int getCustomersDate(int i) {
        if (i > currentDayOfWeek) {
            return i - currentDayOfWeek;
        } else if (i < currentDayOfWeek) {
            return (7 - currentDayOfWeek) + i;
        }
        return 0;
    }

    public static int getDayWeekFromString(String i) {
        return Arrays.asList(WEEKDAYS).indexOf(i);
    }

    static String getDayOfWeekFromInt(int i) {
        return WEEKDAYS[i];
    }

    static String getMonthFromInt (int i) {
        if (i < 0) {
            return MONTHS[MONTHS.length - 1];
        }
        return MONTHS[i];
    }
}


package org.bespyatov.keyboards;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



public class TimeInlineKeyboard {
    static String[] TIME_INTERVALS_1 = {"9.00", "9.30", "10.00", "10.30", "11.00", "11.30", "12.00"};
    static String[] TIME_INTERVALS_2 = {"12.30", "13.00", "13.30", "14.00", "14.30", "15.00", "16.30"};
    static String[] TIME_INTERVALS_3 = {"17.00", "17.30", "18.00", "18.30", "19.00", "19.30", "20.00"};
    static Calendar rightNow = Calendar.getInstance();
    static Date date = rightNow.getTime();
    static int currentDayOfWeek = date.getDay();

    public void setButtons (SendMessage sendMessage) {

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboardRowList = new ArrayList<>();
        KeyboardRow firstRow = new KeyboardRow();
        KeyboardRow secondRow = new KeyboardRow();
        KeyboardRow thirdRow = new KeyboardRow();
        KeyboardRow fourthRow = new KeyboardRow();
        KeyboardRow fifthRow = new KeyboardRow();
        KeyboardRow friday = new KeyboardRow();
        KeyboardRow saturday = new KeyboardRow();

        //rows with time intervals


        firstRow.add(TIME_INTERVALS_1[0]);
        firstRow.add(TIME_INTERVALS_1[1]);
        firstRow.add(TIME_INTERVALS_1[2]);
        firstRow.add(TIME_INTERVALS_1[3]);

        secondRow.add(TIME_INTERVALS_1[4]);
        secondRow.add(TIME_INTERVALS_1[5]);
        secondRow.add(TIME_INTERVALS_1[6]);
        secondRow.add(TIME_INTERVALS_2[0]);

        thirdRow.add(TIME_INTERVALS_2[1]);
        thirdRow.add(TIME_INTERVALS_2[2]);
        thirdRow.add(TIME_INTERVALS_2[3]);
        thirdRow.add(TIME_INTERVALS_2[4]);

        fourthRow.add(TIME_INTERVALS_2[5]);
        fourthRow.add(TIME_INTERVALS_2[6]);
        fourthRow.add(TIME_INTERVALS_3[0]);
        fourthRow.add(TIME_INTERVALS_3[1]);

        fifthRow.add(TIME_INTERVALS_3[2]);
        fifthRow.add(TIME_INTERVALS_3[3]);
        fifthRow.add(TIME_INTERVALS_3[4]);
        fifthRow.add(TIME_INTERVALS_3[5]);





        keyboardRowList.add(firstRow);
        keyboardRowList.add(secondRow);
        keyboardRowList.add(thirdRow);
        keyboardRowList.add(fourthRow);
        keyboardRowList.add(fifthRow);
//        keyboardRowList.add(friday);
//        keyboardRowList.add(saturday);

        replyKeyboardMarkup.setKeyboard(keyboardRowList);
    }
}
